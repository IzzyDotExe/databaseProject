CREATE TABLE ClassType ( --done
    type_id varchar(6),
    title varchar(30) NOT NULL,

    constraint CT_PK PRIMARY KEY (type_id) 
);

create type ctType as object (
    type_id varchar(6),
    title varchar(30)
);
/

CREATE TABLE Term ( --done
    term_id varchar(6),
    season varchar(30) NOT NULL,
    year number(4) NOT NULL,
    
    CONSTRAINT TERM_PK PRIMARY KEY (term_id)
);

create type termType as object (
    term_id varchar(6),
    season varchar(30),
    year number(4)
);
/

CREATE TABLE Domain ( --done
    domain_id varchar(6),
    title varchar(30) NOT NULL,
    
    CONSTRAINT DOM_PK PRIMARY KEY (domain_id)
);

create type domainType as object (
    domain_id varchar(6),
    title varchar(30)
);
/

CREATE TABLE Competency ( --done
    comp_id varchar(6) PRIMARY KEY,
    description varchar(200)
);

create type competencyType as object (
    comp_id varchar(6),
    description varchar(200)
);
/

CREATE TABLE Elements ( --done
    elem_id varchar(6) PRIMARY KEY,
    name varchar(500),
    description varchar(1500)
);

create type ElementType as object (
    elem_id varchar(6),
    name varchar(500),
    description varchar(1500)
);
/

CREATE TABLE CompElem (
    comp_id varchar(6),
    elem_id varchar(6),
    compelem_index number(2),
    
    constraint CPE_FK1 FOREIGN KEY (comp_id) REFERENCES Competency(comp_id) ON DELETE CASCADE,
    constraint CPE_FK2 FOREIGN KEY (elem_id) REFERENCES Elements(elem_id) ON DELETE CASCADE
);

CREATE TABLE Course ( --done

    course_id varchar(10),
    course_description varchar(500),
    course_title varchar(30),
    course_hours number(3),
    ponderation varchar(5),
    credits number,
    term_id varchar(6),
    domain_id varchar(6),
    type_id varchar(6),

    CONSTRAINT CR_PK PRIMARY KEY (course_id),
    CONSTRAINT CR_FK1 FOREIGN KEY (term_id) REFERENCES Term(term_id) ON DELETE CASCADE,
    CONSTRAINT CR_FK2 FOREIGN KEY (domain_id) REFERENCES Domain(domain_id) ON DELETE CASCADE,
    CONSTRAINT CR_FK3 FOREIGN KEY (type_id) REFERENCES ClassType(type_id) ON DELETE CASCADE

);

create type courseType as object (
    course_id varchar(10),
    course_description varchar(500),
    course_title varchar(30),
    course_hours number(3),
    ponderation varchar(5),
    credits number,
    term_id varchar(6),
    domain_id varchar(6),
    type_id varchar(6)
);
/

CREATE TABLE CourseElem (

    course_id varchar(10),
    elem_id varchar(6),
    hours number,
    
    constraint CEE_FK1 FOREIGN KEY (course_id) REFERENCES Course(course_id) ON DELETE CASCADE,
    constraint CEE_FK2 FOREIGN KEY (elem_id) REFERENCES Elements(elem_id) ON DELETE CASCADE
    
);

CREATE OR REPLACE TYPE INTARR IS TABLE OF  NUMBER;
