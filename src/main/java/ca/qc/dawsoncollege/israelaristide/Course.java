package ca.qc.dawsoncollege.israelaristide;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class Course implements SQLData{
    public static final String TYPENAME = "COURSETYPE";

    public static void getCourse(Connection conn){
        try{
            Scanner scan = new Scanner(System.in);
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Course WHERE course_id = ?");
            System.out.println("what course id are you selecting?");
            stmt.setString(1, scan.nextLine());
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                System.out.println("course name: " + rs.getString(2));
            }
        }
        catch(SQLException e){
            System.out.println(e);
        }
        
    }

    private String course_id;

    public String getCourse_id() {
        return course_id;
    }
    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    private String course_description;
    public String getCourse_description() {
        return course_description;
    }
    public void setCourse_description(String course_description) {
        this.course_description = course_description;
    }

    private String course_title;
    public String getCourse_title() {
        return course_title;
    }
    public void setCourse_title(String course_title) {
        this.course_title = course_title;
    }

    private int course_hours;
    public int getCourse_hours() {
        return course_hours;
    }
    public void setCourse_hours(int course_hours) {
        this.course_hours = course_hours;
    }

    private String ponderation;
    public String getPonderation() {
        return ponderation;
    }
    public void setPonderation(String ponderation) {
        this.ponderation = ponderation;
    }

    private double credits;
    public double getCredits() {
        return credits;
    }
    public void setCredits(double credits) {
        this.credits = credits;
    }

    private String term_id;
    public String getTerm_id() {
        return term_id;
    }
    public void setTerm_id(String term_id) {
        this.term_id = term_id;
    }

    private String domain_id;
    public String getDomain_id() {
        return domain_id;
    }
    public void setDomain_id(String domain_id) {
        this.domain_id = domain_id;
    }

    private String type_id;
    public String getType_id() {
        return type_id;
    }
    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public Course(){
        
    }

    public Course(String course_id, String course_description, String course_title, int course_hours,
            String ponderation, double credits, String term_id, String domain_id, String type_id) {
        this.course_id = course_id;
        this.course_description = course_description;
        this.course_title = course_title;
        this.course_hours = course_hours;
        this.ponderation = ponderation;
        this.credits = credits;
        this.term_id = term_id;
        this.domain_id = domain_id;
        this.type_id = type_id;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        
        return Course.TYPENAME;
    }
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setCourse_id(stream.readString());
        setCourse_description(stream.readString());
        setCourse_title(stream.readString());
        setCourse_hours(stream.readInt());
        setPonderation(stream.readString());
        setCredits(stream.readDouble());
        setTerm_id(stream.readString());
        setDomain_id(stream.readString());
        setType_id(stream.readString());
    }
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {

        stream.writeString(getCourse_id());
        stream.writeString(getCourse_description());
        stream.writeString(getCourse_title());
        stream.writeInt(getCourse_hours());
        stream.writeString(getPonderation());
        stream.writeDouble(getCredits());
        stream.writeString(getTerm_id());
        stream.writeString(getDomain_id());
        stream.writeString(getType_id());
    }
    @Override
    public String toString() {
        return "Course [course_id=" + course_id + ", course_description=" + course_description + ", course_title="
                + course_title + ", course_hours=" + course_hours + ", ponderation=" + ponderation + ", credits="
                + credits + ", term_id=" + term_id + ", domain_id=" + domain_id + ", type_id=" + type_id + "]";
    }

    public void getInput(Connection conn){
        Scanner scan = new Scanner(System.in);

        System.out.print("input for course id: ");
        this.course_id = scan.nextLine();
        System.out.flush();

        System.out.print("input for course description: ");
        this.course_description = scan.nextLine();
        System.out.flush();

        System.out.print("input for course title: ");
        this.course_title = scan.nextLine();
        System.out.flush();

        System.out.print("input for course hours: ");
        this.course_hours = scan.nextInt();
        scan.nextLine();
        System.out.flush();

        System.out.print("input for theory hours: ");
        int theory = scan.nextInt();
        scan.nextLine();
        System.out.flush();

        System.out.print("input for lab hours: ");
        int lab = scan.nextInt();
        scan.nextLine();
        System.out.flush();

        System.out.print("input for homework hours: ");
        int homework = scan.nextInt();
        scan.nextLine();
        System.out.flush();

        this.credits = (theory + lab + homework) / 3d;
        this.ponderation = theory + "-" + lab + "-" + homework;
        // TODO: SELECT MENU

        System.out.println("Select which term you would like to add this course to. ");
        ArrayList<String> terms = ListFunctions.listTerms(conn);
        if (terms.size() != 0) {
            setTerm_id(terms.get(scan.nextInt() -1));
            System.out.flush();
        } else {
            SqlCLI.setCurrentException(new Exception("Please create some terms before creating a course."));
            return;
        }
        scan.nextLine();

        System.out.println("Select which domain you would like to add this course to. ");
        ArrayList<String> doms = ListFunctions.listDomains(conn);
        if (doms.size() != 0) {
            setDomain_id(doms.get(scan.nextInt() -1)); 
            System.out.flush();
        } else {
            SqlCLI.setCurrentException(new Exception("Please create some domains before creating a course."));
            return;
        }
        scan.nextLine();

        System.out.println("Select which type you would like to add this course to. ");
        ArrayList<String> types = ListFunctions.listClassTypes(conn);
        if (types.size() != 0) {
            setType_id(types.get(scan.nextInt() -1));
            System.out.flush();
        } else {
            SqlCLI.setCurrentException(new Exception("Please create some class types before creating a course."));
            return;
        }
        scan.nextLine();

        addToDatabase(conn);
    }
    
    public void addToDatabase(Connection conn){
        try{
            Map map = conn.getTypeMap();
            map.put(Course.TYPENAME, 
            // this is the package and the class name!
            Class.forName("ca.qc.dawsoncollege.israelaristide.Course"));
            conn.setTypeMap(map);

            String addProc = "{ call crud.create_course(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setObject(1, this);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException | ClassNotFoundException e){
            SqlCLI.setCurrentException(new Exception(e.getMessage() + "\n" + this));
        }
    }
    public static void removeFromDatabase(Connection conn, String input){
        try{
            String addProc = "{ call crud.delete_course(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setString(1, input);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
    }

    public static void addElements(Connection conn, String course_id) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Select a competency to add elements from to the course. ");
        ArrayList<String> compselects = ListFunctions.listCompetencies(conn);
        String compid;
        if (compselects.size() != 0)
            compid = compselects.get(scan.nextInt() - 1);
        else {
            SqlCLI.setCurrentException(new Exception("There are no competencies to delete elements from."));
            return;
        }
        scan.nextLine();

        System.out.println("Select the elements to add from this competency, seperated by a comma.");
        ArrayList<String> elems = ListFunctions.listElements(conn, compid);
        String elemstoAdd;
        ArrayList<Integer> ints = new ArrayList<Integer>();
        if (elems.size() != 0) {

            elemstoAdd = scan.nextLine();

            String[] elemsSelected = elemstoAdd.split(",");

            try {
                for (String elem : elemsSelected) {
                    ints.add(Integer.parseInt(elem));
                }
            } catch (Exception ex) {
                SqlCLI.setCurrentException(ex);
                return;
            }
        }
        else {
            SqlCLI.setCurrentException(new Exception("There are no elements on this competency."));
            return;
        }

        System.out.print("Input the ammount of hours to add: ");
        int hours = scan.nextInt();
        scan.nextLine();
        System.out.flush();

        try {
            ArrayDescriptor ad = new ArrayDescriptor("INTARR", conn);
            ARRAY arr = new ARRAY(ad, conn, ints.toArray());
            String addProc = "{ call crud.create_course_elements(?, ? , ?, ?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setString(1, course_id);
            stmt.setString(2, compid);
            stmt.setInt(3, hours);
            stmt.setArray(4, arr);
            stmt.execute();
        } catch (SQLException e) {
            SqlCLI.setCurrentException(e);
        }

    }


    public static void removeElements(Connection conn, String course_id) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Select a competency to delete elements from in the course. ");
        ArrayList<String> compselects = ListFunctions.listCompetencies(conn);
        String compid;
        if (compselects.size() != 0)
            compid = compselects.get(scan.nextInt() - 1);
        else {
            SqlCLI.setCurrentException(new Exception("There are no competencies to delete elements from."));
            return;
        }
        scan.nextLine();

        System.out.println("Select the elements to delete from this competency, seperated by a comma.");
        ArrayList<String> elems = ListFunctions.listCourseElements(conn, compid, course_id);
        String elemstodelete;
        ArrayList<Integer> ints = new ArrayList<Integer>();
        if (elems.size() != 0) {

            elemstodelete = scan.nextLine();

            String[] elemsSelected = elemstodelete.split(",");

            try {
                for (String elem : elemsSelected) {
                    ints.add(Integer.parseInt(elem));
                }
            } catch (Exception ex) {
                SqlCLI.setCurrentException(ex);
                return;
            }
        }
        else {
            SqlCLI.setCurrentException(new Exception("There are no elements on this competency."));
            return;
        }

        try {
            ArrayDescriptor ad = new ArrayDescriptor("INTARR", conn);
            ARRAY arr = new ARRAY(ad, conn, ints.toArray());
            String addProc = "{ call crud.delete_course_elements(?, ? , ?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setString(1, course_id);
            stmt.setString(2, compid);
            stmt.setArray(3, arr);
            stmt.execute();
        } catch (SQLException e) {
            SqlCLI.setCurrentException(e);
        }

    }

    public static void updateDatabase(Connection conn, Course input){
        try{
            String addProc = "{ call crud.update_course(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setObject(1, input);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
    }
    public void getInputForUpdate(Connection conn){
        int theory = 0;
        int lab = 0;
        int homework = 0;
        Scanner scan = new Scanner(System.in);

        System.out.print("input for course id: ");
        this.course_id = scan.nextLine();
        System.out.flush();


        System.out.print("input for course description: ");
        this.course_description = scan.nextLine();
        System.out.flush();


        System.out.print("input for course title: ");
        this.course_title = scan.nextLine();
        System.out.flush();


        System.out.print("input for course hours: ");
        this.course_hours = scan.nextInt();
        scan.nextLine();
        System.out.flush();


        System.out.print("input for theory hours: ");
        theory = scan.nextInt();
        scan.nextLine();
        System.out.flush();


        System.out.print("input for lab hours: ");
        lab = scan.nextInt();
        scan.nextLine();
        System.out.flush();

        System.out.print("input for homework hours: ");
        homework = scan.nextInt();
        scan.nextLine();
        System.out.flush();


        System.out.println("Select which term you would like to add this course to. ");
        ArrayList<String> terms = ListFunctions.listTerms(conn);
        if (terms.size() != 0) {
            setTerm_id(terms.get(scan.nextInt() -1));
            System.out.flush();
        } else {
            SqlCLI.setCurrentException(new Exception("Please create some terms before creating a course."));
            return;
        }
        scan.nextLine();


        System.out.println("Select which domain you would like to add this course to. ");
        ArrayList<String> doms = ListFunctions.listDomains(conn);
        if (doms.size() != 0) {
            setDomain_id(doms.get(scan.nextInt() -1));
            System.out.flush();
        } else {
            SqlCLI.setCurrentException(new Exception("Please create some domains before creating a course."));
            return;
        }
        scan.nextLine();


        System.out.println("Select which type you would like to add this course to. ");
        ArrayList<String> types = ListFunctions.listClassTypes(conn);
        if (types.size() != 0) {
            setType_id(types.get(scan.nextInt() -1));
            System.out.flush();
        } else {
            SqlCLI.setCurrentException(new Exception("Please create some class types before creating a course."));
            return;
        }
        scan.nextLine();

        this.credits = (theory + lab + homework) / 3d;
        this.ponderation = theory + "-" + lab + "-" + homework;
        updateDatabase(conn, this);
    }
}
    
