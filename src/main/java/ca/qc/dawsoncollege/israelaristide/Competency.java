package ca.qc.dawsoncollege.israelaristide;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Map;
import java.util.Scanner;

public class Competency implements SQLData{
    private String comp_id;

    public String getComp_id() {
        return comp_id;
    }

    public void setComp_id(String comp_id) {
        this.comp_id = comp_id;
    }

    private String description;
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static final String TYPENAME = "COMPETENCYTYPE";

    public static void getCompetency(Connection conn){
        try{
            Scanner scan = new Scanner(System.in);
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Competency WHERE comp_id = ?");
            System.out.println("what competency id are you selecting?");
            stmt.setString(1, scan.nextLine());
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                System.out.println("competency name: " + rs.getString(2));
            }
        }
        catch(SQLException e){
            System.out.println(e);
        }
        
    }

    public Competency(){
        
    }

    public Competency(String comp_id, String description) {
        this.comp_id = comp_id;
        this.description = description;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        
        return Competency.TYPENAME;
    }
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setComp_id(stream.readString());
        setDescription(stream.readString());
    }
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {

        stream.writeString(getComp_id());
        stream.writeString(getDescription());
    }
    

    @Override
    public String toString() {
        return "Competency [comp_id=" + comp_id + ", description=" + description + "]";
    }

    public void getInput(Connection conn){

        Scanner scan = new Scanner(System.in);
        System.out.print("Input the competency ID: ");
        this.comp_id = scan.nextLine();
        System.out.flush();

        System.out.print("Input the competency's short Description: ");
        this.description = scan.nextLine();
        System.out.flush();

        addToDatabase(conn);

    }
    
    public void addToDatabase(Connection conn){
        try{
            Map map = conn.getTypeMap();
            map.put(Competency.TYPENAME, 
            // this is the package and the class name!
            Class.forName("ca.qc.dawsoncollege.israelaristide.Competency"));
            conn.setTypeMap(map);

            String addProc = "{ call crud.create_competency(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setObject(1, this);
            stmt.execute();
            SqlCLI.setCurrentException(null);

        } catch(SQLException | ClassNotFoundException e){
            SqlCLI.setCurrentException(e);
        }

    }
    public static void removeFromDatabase(Connection conn, String input){
        try{
            String addProc = "{ call crud.delete_competency(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setString(1, input);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
    }
    public static void updateDatabase(Connection conn, Competency input){
        try{
            String addProc = "{ call crud.update_competency(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setObject(1, input);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
    }
    public void getInputForUpdate(Connection conn){
        Scanner scan = new Scanner(System.in);

        System.out.print("Input the competency's new short Description: ");
        this.description = scan.nextLine();
        System.out.flush();
        
        updateDatabase(conn, this);
    }
}


    
