package ca.qc.dawsoncollege.israelaristide;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Map;
import java.util.Scanner;

public class Term implements SQLData{
    public static final String TYPENAME = "TERMTYPE";

    public static void getTerm(Connection conn){
        try{
            Scanner scan = new Scanner(System.in);
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Term WHERE term_id = ?");
            System.out.println("what Term id are you selecting?");
            stmt.setString(1, scan.nextLine());
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                System.out.println("Term name: " + rs.getString(2));
            }
        }
        catch(SQLException e){
            System.out.println(e);
        }
        
    }

    private String term_id; 

    public String getTerm_id() {
        return term_id;
    }

    public void setTerm_id(String term_id) {
        this.term_id = term_id;
    }

    private String season;
    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    private int year;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Term(){
        
    }

    public Term(String term_id, String season, int year) {
        this.term_id = term_id;
        this.season = season;
        this.year = year;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        
        return Term.TYPENAME;
    }
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setTerm_id(stream.readString());
        setSeason(stream.readString());
        setYear(stream.readInt());
    }
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {

        stream.writeString(getTerm_id());
        stream.writeString(getSeason());
        stream.writeInt(getYear());
    }

    public void getInput(Connection conn){

        Scanner scan = new Scanner(System.in);

        System.out.print("Input for season: ");
        this.season = scan.nextLine();
        System.out.flush();

        System.out.print("input for year: ");
        this.year = scan.nextInt();
        System.out.flush();

        addToDatabase(conn);
    }
    
    public void addToDatabase(Connection conn){
        try{
            Map map = conn.getTypeMap();
            map.put(Term.TYPENAME, 
            // this is the package and the class name!
            Class.forName("ca.qc.dawsoncollege.israelaristide.Term"));
            conn.setTypeMap(map);

            String addProc = "{ call crud.create_term(?)}";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setObject(1, this);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException | ClassNotFoundException e){
            SqlCLI.setCurrentException(e);
        }
    }
    public static void removeFromDatabase(Connection conn, String input){
        try{
            String addProc = "{ call crud.delete_term(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setString(1, input);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
    }
    public static void updateDatabase(Connection conn, Term input){
        try{
            String addProc = "{ call crud.update_term(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setObject(1, input);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
    }
    public void getInputForUpdate(Connection conn){

        Scanner scan = new Scanner(System.in);

        System.out.print("Input for season: ");
        this.season = scan.nextLine();
        System.out.flush();


        System.out.print("input for year: ");
        this.year = scan.nextInt();
        System.out.flush();
        
        updateDatabase(conn, this);

    }
}
