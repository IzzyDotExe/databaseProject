package ca.qc.dawsoncollege.israelaristide;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class Elements implements SQLData{
    public static final String TYPENAME = "ELEMENTTYPE";

    public static void getElement(Connection conn){
        try{
            Scanner scan = new Scanner(System.in);
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Elements WHERE elem_id = ?");
            System.out.println("what element id are you selecting?");
            stmt.setString(1, scan.nextLine());
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                System.out.println("element name: " + rs.getString(2));
            }
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
        
    }

    private String elem_id;

    public String getElem_id() {
        return elem_id;
    }

    public void setElem_id(String elem_id) {
        this.elem_id = elem_id;
    }

    private String name;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public Elements(){
        
    }

    public Elements(String elem_id, String name, String description) {
        this.elem_id = elem_id;
        this.name = name;
        this.description = description;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        
        return Elements.TYPENAME;
    }
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setElem_id(stream.readString());
        setName(stream.readString());
        setDescription(stream.readString());
    }
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {

        stream.writeString(getElem_id());
        stream.writeString(getName());
        stream.writeString(getDescription());
    }
    

    @Override
    public String toString() {
        return "Elements [elem_id=" + elem_id + ", name=" + name + ", description=" + description + "]";
    }

    public void getInput(Connection conn){
        Scanner scan = new Scanner(System.in);

        System.out.println("Select a competency to list elements from. ");
        ArrayList<String> compselects = ListFunctions.listCompetencies(conn);
        String compid;

        if (compselects.size() != 0)
            compid = compselects.get(scan.nextInt() - 1);
        else {
            SqlCLI.setCurrentException(new Exception("Please create a competency before creating elements.  "));
            return;
        }
        scan.nextLine();

        System.out.print("Input for element name: ");
        this.name = scan.nextLine();
        System.out.flush();

        System.out.println("(press enter twice to submit, enter once to make a new line)");
        System.out.print("Input for element description: ");
        this.description = SqlCLI.readMultiLine(scan);
        System.out.flush();

        addToDatabase(conn, compid);
    }
    
    public void addToDatabase(Connection conn, String comp_id){
        try{
            Map map = conn.getTypeMap();
            map.put(Elements.TYPENAME, 
            // this is the package and the class name!
            Class.forName("ca.qc.dawsoncollege.israelaristide.Elements"));
            conn.setTypeMap(map);

            String addProc = "{ call crud.create_element(?, ?)}";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setString(1, comp_id);
            stmt.setObject(2, this);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException | ClassNotFoundException e){
            SqlCLI.setCurrentException(e);
        }
    }
    public static void removeFromDatabase(Connection conn, String input){
        try{
            String addProc = "{ call crud.delete_element(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setString(1, input);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
    }
    public static void updateDatabase(Connection conn, Elements input){
        try{
            String addProc = "{ call crud.update_element(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setObject(1, input);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
    }
    public void getInputForUpdate(Connection conn){
        Scanner scan = new Scanner(System.in);

        System.out.print("Input for new element name: ");
        this.name = scan.nextLine();
        System.out.flush();

        System.out.println("(press enter twice to submit, enter once to make a new line)");
        System.out.print("Input for new element description: ");
        this.description = SqlCLI.readMultiLine(scan);
        System.out.flush();
        
        updateDatabase(conn, this);

    }
}
