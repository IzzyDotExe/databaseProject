package ca.qc.dawsoncollege.israelaristide;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SqlCLI
{


    private static String username;
    private static String password;
    private static Connection conn;
    private static Exception currentException = null;

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        SqlCLI.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        SqlCLI.password = password;
    }

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void setCurrentException(Exception currentException) {
        SqlCLI.currentException = currentException;
    }

    public static Exception getCurrentException() {
        return currentException;
    }

    public static void main( String[] args ) throws SQLException, ClassNotFoundException
    {
        userLogin();
        readLoop();
    }

    public static String readMultiLine(Scanner scanner) {

        List<String> tokens = new ArrayList<String>();

        while (true) {
            if (tokens.size() != 0 )
                if (tokens.get(tokens.size()-1).isBlank())
                    break;
            tokens.add(scanner.nextLine());
            System.out.flush();
        }
        StringBuilder sb = new StringBuilder();
        tokens.forEach(x -> sb.append(x).append("\n"));

        return sb.toString();
    }

    private static void printError(Exception ex) {
        
        if (ex == null) {
            System.out.println("");
            return;
        }

        if (ex.getMessage() == "null") {
            System.out.println(ex.getCause().getMessage());
            return;
        }

        System.out.println(ex.getMessage());

    }

    private static void printmenu() {
        printError(currentException);
        System.out.println("==================================");
        System.out.println("Select from the options below");
        System.out.println("1. CREATE");
        System.out.println("2. UPDATE");
        System.out.println("3. REMOVE");
        System.out.println("4. DISPLAY");
        System.out.println("5. VALIDATE DATA");
        System.out.println("6. EXIT");
        System.out.println("=================================");
    }


    private static void printcreate() {
        printError(currentException);
        System.out.println("==================================");
        System.out.println("Select an item to create");
        System.out.println("1. Class Type");
        System.out.println("2. Competency");
        System.out.println("3. Competency Element");
        System.out.println("4. Term");
        System.out.println("5. Domain");
        System.out.println("6. Course");
        System.out.println("7. Course Elements");
        System.out.println("8. exit");
        System.out.println("=================================");
    }
    private static void printDelete() {
        printError(currentException);
        System.out.println("==================================");
        System.out.println("Select an item to delete");
        System.out.println("1. Class Type");
        System.out.println("2. Competency");
        System.out.println("3. Competency Element");
        System.out.println("4. Term");
        System.out.println("5. Domain");
        System.out.println("6. Course");
        System.out.println("7. Course Elements");
        System.out.println("8. exit");
        System.out.println("=================================");
    }

    private static void printDisplay() {
        printError(currentException);
        System.out.println("==================================");
        System.out.println("Here you may view many types of useful info");
        System.out.println("1. Competency Info");
        System.out.println("2. Courses Info ");
        System.out.println("3. Course Elements Info");
        System.out.println("4. Avalible Terms");
        System.out.println("5. Avalible Domains");
        System.out.println("6. Avalible Class Types");
        System.out.println("7. Avalible Competencies and Elements");
        System.out.println("8. EXIT");
        System.out.println("=================================");
    }

    private static void printUpdate() {
        printError(currentException);
        System.out.println("==================================");
        System.out.println("Select an item to update");
        System.out.println("1. Class Type");
        System.out.println("2. Competency");
        System.out.println("4. Term");
        System.out.println("5. Domain");
        System.out.println("6. Course");
        System.out.println("7. exit");
        System.out.println("=================================");
    }

    private static void Validate() throws SQLException {

        ArrayList<String> courses = ListFunctions.listCoursesHeadless(conn);

        for (String courseid : courses) {

            PreparedStatement stmt = conn.prepareStatement("SELECT VALIDATION.VALIDATE_CID(?) as OUT FROM DUAL");
            stmt.setString(1, courseid);
            ResultSet rs = stmt.executeQuery();
            int out = 0;
            if (rs.next()) {
                 out = rs.getInt("OUT");
            }

            if (out == 1) {
                System.out.println(courseid + ": VALID");
            } else {
                System.out.println(courseid + ": INVALID");
                PreparedStatement stmt2 = conn.prepareStatement("SELECT VALIDATION.EXPECTED_CREDITS(?) as OUT FROM DUAL");
                stmt2.setString(0, courseid);
                ResultSet rs2 = stmt.executeQuery();
                if (rs.next()) {
                    System.out.println("exprected credits: " + rs.getInt("OUT"));
                }
            }

        }
        Scanner scan = new Scanner(System.in);
        scan.nextLine();
    }

    public static Connection getConnection(String user, String password) throws SQLException {
        Connection conn = null;
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        conn = DriverManager.getConnection(url, user, password);
        return conn;
    }

    private static void userLogin() {
        String status = "";
        while (true) {
            clearScreen();
            System.out.println(status);
            Scanner scan = new Scanner(System.in);
            System.out.print("please input username: ");
            setUsername(scan.nextLine());
            System.out.flush();
            System.out.print("please input password: ");
            var pwd = System.console().readPassword();
            setPassword(new String(pwd));
            System.out.flush();

            if((username == null || username.trim().isEmpty()) || (password == null || password.trim().isEmpty())) {

                status = "Invalid username or password";

            } else {
                try {
                    conn = getConnection(username, password);
                    break;
                } catch (SQLException e) {
                    status = "Username and password were rejected by the database.";
                }
            }

        }
    }

    private static void createMenu() {

        boolean running = true;
        String status = "";
        System.out.println("Create menu");

        while(running) {

            clearScreen();
            System.out.println(status);
            printcreate();
            Scanner scan = new Scanner(System.in);
            var input = scan.nextLine();
            try {


                switch (input) {
                    case "1":
                        ClassType ct = new ClassType();
                        ct.getInput(conn);
                        break;
                    case "2":
                        Competency comp = new Competency();
                        comp.getInput(conn);
                        break;
                    case "3":
                        Elements elem = new Elements();
                        elem.getInput(conn);
                        break;
                    case "4":
                        Term term = new Term();
                        term.getInput(conn);
                        break;
                    case "5":
                        Domain dom = new Domain();
                        dom.getInput(conn);
                        break;
                    case "6":
                        Course crs = new Course();
                        crs.getInput(conn);
                        break;
                    case "7":
                        System.out.println("which Course would you like to add elements to?");
                        ArrayList<String> coursesToAdd = ListFunctions.listCourses(conn);
                        if (coursesToAdd.size() != 0) {
                            Course.addElements(conn, coursesToAdd.get(scan.nextInt() - 1));
                            scan.nextLine();
                        }
                        else
                            status = "There are no courses to delete";

                        break;
                    case "8":
                        running = false;
                        break;
                    default:
                        System.out.println("option not found, pick another one");
                        break;
                }
            } catch (Exception ex) {
                currentException = ex;
            }

        }
    }

    private static void deleteMenu() {

        boolean running = true;
        String status = "";
        System.out.println("Delete menu");

        while(running) {

            clearScreen();
            System.out.println(status);
            printDelete();
            Scanner scan = new Scanner(System.in);

            var input = scan.nextLine();

            switch (input) {
                case "1":
                    System.out.println("which class type would you like to delete?");
                    ArrayList<String> ctToDelete = ListFunctions.listClassTypes(conn);
                    if (ctToDelete.size() != 0)
                        ClassType.removeFromDatabase(conn, ctToDelete.get(scan.nextInt()-1));
                    else
                        status = "There are no class types to delete";

                    break;
                case "2":
                    System.out.println("which competency would you like to delete?");
                    ArrayList<String> competencyToDelete = ListFunctions.listCompetencies(conn);
                    if (competencyToDelete.size() != 0)
                        Competency.removeFromDatabase(conn, competencyToDelete.get(scan.nextInt()-1));
                    else
                        status = "There are no competencies to delete.";

                    break;
                case "3":
                    System.out.println("Select a competency to list elements from. ");
                    ArrayList<String> compselects = ListFunctions.listCompetencies(conn);
                    String compid;
                    if (compselects.size() != 0)
                        compid = compselects.get(scan.nextInt() - 1);
                    else {
                        status = "There are no competencies to delete elements from.";
                        break;
                    }

                    scan.nextLine();
                    System.out.println("Select an element to delete from this competency. ");
                    ArrayList<String> elem = ListFunctions.listElements(conn, compid);
                    if (elem.size() != 0)
                        Elements.removeFromDatabase(conn, elem.get(scan.nextInt() -1));
                    else
                        status = "There are no elements on this competency.";

                    break;
                case "4":
                    System.out.println("which Term would you like to delete?");
                    ArrayList<String> termToDelete = ListFunctions.listTerms(conn);
                    if (termToDelete.size() != 0)
                        Term.removeFromDatabase(conn, termToDelete.get(scan.nextInt()-1));
                    else
                        status = "There are no terms to delete.";
                    break;
                case "5":
                    System.out.println("which Domain would you like to delete?");
                    ArrayList<String> domainToDelete = ListFunctions.listDomains(conn);
                    if (domainToDelete.size() != 0)
                        Domain.removeFromDatabase(conn, domainToDelete.get(scan.nextInt()-1));
                    else
                        status = "There are no domains to delete.";
                    break;
                case "6":
                    System.out.println("which Course would you like to delete?");
                    ArrayList<String> courseToDelete = ListFunctions.listCourses(conn);
                    if (courseToDelete.size() != 0)
                        Course.removeFromDatabase(conn, courseToDelete.get(scan.nextInt()-1));
                    else
                        status = "There are no courses to delete";

                    break;
                case "7":

                    System.out.println("which Course would you like to delete elements from?");
                    ArrayList<String> coursesToAdd = ListFunctions.listCourses(conn);
                    if (coursesToAdd.size() != 0) {
                        Course.removeElements(conn, coursesToAdd.get(scan.nextInt() - 1));
                        scan.nextLine();
                    }

                    break;
                case "8":
                    running = false;
                    break;
                default:
                    System.out.println("option not found, pick another one");
                    break;
            }
        }
    }
    private static void updateMenu() {

        boolean running = true;
        String status = "";
        System.out.println("Update menu");

        while(running) {

            clearScreen();
            System.out.println(status);
            printUpdate();
            Scanner scan = new Scanner(System.in);

            var input = scan.nextLine();

            switch (input) {
                case "1":
                    System.out.println("which class type would you like to update?");
                    ArrayList<String> ctToUpdate = ListFunctions.listClassTypes(conn);
                    if (ctToUpdate.size() != 0) {
                        ClassType ct = new ClassType();
                        ct.setType_id(ctToUpdate.get(scan.nextInt() - 1));
                        scan.nextLine();
                        ct.getInputForUpdate(conn);
                    }
                    else
                        status = "There are no class types to update";

                    break;
                case "2":
                    System.out.println("which competency would you like to update?");
                    ArrayList<String> competencyToUpdate = ListFunctions.listCompetencies(conn);
                    if (competencyToUpdate.size() != 0) {
                        Competency comp = new Competency();
                        comp.setComp_id(competencyToUpdate.get(scan.nextInt() - 1));
                        scan.nextLine();
                        comp.getInputForUpdate(conn);

                    }
                    else
                        status = "There are no competencies to update.";

                    break;
                case "3":
                    System.out.println("Select a competency to list elements from. ");
                    ArrayList<String> compselects = ListFunctions.listCompetencies(conn);
                    String compid;
                    if (compselects.size() != 0)
                        compid = compselects.get(scan.nextInt() - 1);
                    else {
                        status = "There are no competencies to update elements from.";
                        break;
                    }

                    scan.nextLine();
                    System.out.println("Select an element to update from this competency. ");
                    ArrayList<String> elem = ListFunctions.listElements(conn, compid);
                    if (elem.size() != 0) {
                        Elements elemc = new Elements();
                        elemc.setElem_id(elem.get(scan.nextInt() - 1 ));
                        scan.nextLine();
                        elemc.getInputForUpdate(conn);
                    }
                    else
                        status = "There are no elements on this competency.";

                    break;
                case "4":
                    System.out.println("which Term would you like to update?");
                    ArrayList<String> termToUpdate = ListFunctions.listTerms(conn);
                    if (termToUpdate.size() != 0) {
                        Term tmr = new Term();
                        tmr.setTerm_id(termToUpdate.get(scan.nextInt() - 1));
                        scan.nextLine();
                        tmr.getInputForUpdate(conn);

                    }
                    else
                        status = "There are no terms to update.";
                    break;
                case "5":
                    System.out.println("which Domain would you like to update?");
                    ArrayList<String> domainToUpdate = ListFunctions.listDomains(conn);
                    if (domainToUpdate.size() != 0)
                    {
                        Domain dom = new Domain();
                        dom.setDomain_id(domainToUpdate.get(scan.nextInt() - 1));
                        scan.nextLine();
                        dom.getInputForUpdate(conn);
                    }
                    else
                        status = "There are no domains to update.";
                    break;
                case "6":
                    System.out.println("which Course would you like to update?");
                    ArrayList<String> courseToUpdate = ListFunctions.listCourses(conn);
                    if (courseToUpdate.size() != 0)
                    {
                        Course crs = new Course();
                        crs.setCourse_id(courseToUpdate.get(scan.nextInt() - 1));
                        scan.nextLine();
                        crs.getInputForUpdate(conn);
                    }
                    else
                        status = "There are no courses to update";

                    break;
                case "7":
                    running = false;
                    break;
                default:
                    System.out.println("option not found, pick another one");
                    break;
            }
        }
    }


    private static void readLoop() {

        boolean running = true;
        String status = "";
        System.out.println("Welcome " + username + " to the DB CLI!");
        
        while(running) {

            clearScreen();
            System.out.println(status);
            printmenu();
            Scanner scan = new Scanner(System.in);
            var input = scan.nextLine();

            switch (input) {
                case "1":
                    createMenu();
                break;
                case "2":
                    updateMenu();
                break;
                case "3":
                    deleteMenu();
                    break;
                case "4":
                    displayMenu();
                    break;
                case "7":
                    try {
                        Validate();
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                    break;
                case "6":
                    System.out.println("Exiting");
                    running = false;
                break;
                default:
                    System.out.println("option not found, pick another one");
                break;
            }


        }
    }

    private static void displayMenu() {

        boolean running = true;
        String status = "";

        while(running) {

            clearScreen();
            System.out.println(status);
            printDisplay();
            Scanner scan = new Scanner(System.in);
            var input = scan.nextLine();
            try {

                switch (input) {
                    case "1":

                        System.out.println("Select a competency to list info about.");
                        ArrayList<String> compselects = ListFunctions.listCompetencies(conn);
                        String compid;
                        if (compselects.size() != 0)
                            compid = compselects.get(scan.nextInt() - 1);
                        else {
                            SqlCLI.setCurrentException(new Exception("Please create a competency before creating elements.  "));
                            return;
                        }
                        scan.nextLine();

                        ViewFunctions.displayCompInfo(conn, compid);
                        System.out.println("Press any key to continue...");
                        scan.nextLine();
                        break;
                    case "2":
                        System.out.println("Select a course to display info about: ");
                        ArrayList<String> courses = ListFunctions.listCourses(conn);
                        if (courses.size() != 0)
                            ViewFunctions.displayCourseInfo(conn, courses.get(scan.nextInt()-1));
                        else
                            status = "There are no courses to display";
                        scan.nextLine();

                        System.out.println("Press any key to continue...");
                        scan.nextLine();
                        break;
                    case "3":

                        System.out.println("Select a course to display info about: ");
                        ArrayList<String> displayCourses = ListFunctions.listCourses(conn);

                        if (displayCourses.size() != 0)
                            ViewFunctions.displayCourseElements(conn, displayCourses.get(scan.nextInt()-1));
                        else
                            status = "There are no courses to display";
                        scan.nextLine();
                        System.out.println("Press any key to continue...");
                        scan.nextLine();
                        break;
                    case "4":
                        ListFunctions.listTerms(conn);
                        System.out.println("Press any key to continue...");
                        scan.nextLine();
                        break;
                    case "5":
                        ListFunctions.listDomains(conn);
                        System.out.println("Press any key to continue...");
                        scan.nextLine();
                        break;
                    case "6":
                        ListFunctions.listClassTypes(conn);
                        System.out.println("Press any key to continue...");
                        scan.nextLine();
                        break;
                    case "7":
                        System.out.println("Select a competency to list elements.");
                        ArrayList<String> compselects2 = ListFunctions.listCompetencies(conn);
                        String compid2;
                        if (compselects2.size() != 0)
                            compid = compselects2.get(scan.nextInt() - 1);
                        else {
                            SqlCLI.setCurrentException(new Exception("Please create a competency before creating elements.  "));
                            return;
                        }
                        scan.nextLine();

                        ListFunctions.listElements(conn, compid);

                        System.out.println("Press any key to continue...");

                        scan.nextLine();
                        break;

                    case "8":
                        running = false;
                        break;
                    default:
                        System.out.println("option not found, pick another one");
                        break;
                }
            } catch (Exception ex) {
                currentException = ex;
            }

        }
    }


}
