package ca.qc.dawsoncollege.israelaristide;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Map;
import java.util.Scanner;

public class ClassType implements SQLData{

    public static final String TYPENAME = "CTTYPE";

    public static void getClassType(Connection conn){
        try{
            Scanner scan = new Scanner(System.in);
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ClassType WHERE type_id = ?");
            System.out.println("what type id are you selecting?");
            stmt.setString(1, scan.nextLine());
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                System.out.println("Class type name: " + rs.getString(2));
            }
        }
        catch(SQLException e){
            System.out.println(e);
        }
        
    }

    private String type_id;
    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ClassType()
    {
        
    }

    public ClassType(String type_id, String title) {
        this.type_id = type_id;
        this.title = title;
    }

    @Override
    public String toString() {
        return "ClassType [type_id=" + type_id + ", title=" + title + "]";
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        
        return ClassType.TYPENAME;
    }
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setType_id(stream.readString());
        setTitle(stream.readString());
    }
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {

        stream.writeString(getType_id());
        stream.writeString(getTitle());
    }

    public void getInput(Connection conn){
        Scanner scan = new Scanner(System.in);

        System.out.print("input for title: ");
        this.title = scan.nextLine();

        addToDatabase(conn);
    }
    
    public void addToDatabase(Connection conn){

        try{

            Map map = conn.getTypeMap();
            map.put(ClassType.TYPENAME, 
            // this is the package and the class name!
            Class.forName("ca.qc.dawsoncollege.israelaristide.ClassType"));
            conn.setTypeMap(map);

            String addProc = "{ call crud.create_classType(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setObject(1, this);
            stmt.execute();
            SqlCLI.setCurrentException(null);

        } catch(SQLException | ClassNotFoundException e) {
            SqlCLI.setCurrentException(e);
        }
    }
    public static void removeFromDatabase(Connection conn, String input){
        try{
            String addProc = "{ call crud.delete_classType(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setString(1, input);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
    }
    public static void updateDatabase(Connection conn, ClassType input){
        try{
            String addProc = "{ call crud.update_classType(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setObject(1, input);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
    }
    public void getInputForUpdate(Connection conn){
        Scanner scan = new Scanner(System.in);

            System.out.print("input for new title: ");
            this.title = scan.nextLine();
        
        updateDatabase(conn, this);
    }


}
