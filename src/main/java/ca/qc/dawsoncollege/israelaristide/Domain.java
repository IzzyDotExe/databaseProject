package ca.qc.dawsoncollege.israelaristide;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Map;
import java.util.Scanner;

public class Domain implements SQLData{
    public static final String TYPENAME = "DOMAINTYPE";

    public static void getDomain(Connection conn){
        try{
            Scanner scan = new Scanner(System.in);
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Domain WHERE domain_id = ?");
            System.out.println("what Domain id are you selecting?");
            stmt.setString(1, scan.nextLine());
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                System.out.println("Domain name: " + rs.getString(2));
            }
        }
        catch(SQLException e){
            System.out.println(e);
        }
        
    }

    private String domain_id;

    public String getDomain_id() {
        return domain_id;
    }
    public void setDomain_id(String domain_id) {
        this.domain_id = domain_id;
    }
    private String title;

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public Domain(){
        
    }

    public Domain(String domain_id, String title) {
        this.domain_id = domain_id;
        this.title = title;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        
        return Domain.TYPENAME;
    }
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setDomain_id(stream.readString());
        setTitle(stream.readString());
    }
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {

        stream.writeString(getDomain_id());
        stream.writeString(getTitle());
    }

    public void getInput(Connection conn){

        Scanner scan = new Scanner(System.in);

        System.out.print("input for title: ");
        this.title = scan.nextLine();
        System.out.flush();

        addToDatabase(conn);
    }
    
    public void addToDatabase(Connection conn){
        try{
            Map map = conn.getTypeMap();
            map.put(Domain.TYPENAME, 
            // this is the package and the class name!
            Class.forName("ca.qc.dawsoncollege.israelaristide.Domain"));
            conn.setTypeMap(map);

            String addProc = "{ call crud.create_domain(?)}";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setObject(1, this);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException | ClassNotFoundException e){
            SqlCLI.setCurrentException(e);
        }
    }
    public static void removeFromDatabase(Connection conn, String input){
        try{
            String addProc = "{ call crud.delete_domain(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setString(1, input);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
    }
    public static void updateDatabase(Connection conn, Domain input){
        try{
            String addProc = "{ call crud.update_domain(?) }";
            CallableStatement stmt = conn.prepareCall(addProc);
            stmt.setObject(1, input);
            stmt.execute();
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
    }
    public void getInputForUpdate(Connection conn){
        Scanner scan = new Scanner(System.in);

        System.out.print("input for new title: ");
        this.title = scan.nextLine();
        System.out.flush();

        updateDatabase(conn, this);
    }
}
