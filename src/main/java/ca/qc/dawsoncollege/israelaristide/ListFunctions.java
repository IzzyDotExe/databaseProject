package ca.qc.dawsoncollege.israelaristide;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class ListFunctions {


    public static ArrayList<String> listCourses(Connection conn){
        ArrayList<String> IDs = new ArrayList<String>();
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT COURSE_ID, COURSE_TITLE FROM COURSE");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                IDs.add(rs.getString(1));
                System.out.println(IDs.size() + ". " + rs.getString(2) );
            }
            
        }
        catch(SQLException e){
            System.out.println(e);
        }
        return IDs;
    }

    public static ArrayList<String> listCoursesHeadless(Connection conn){
        ArrayList<String> IDs = new ArrayList<String>();
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT COURSE_ID, COURSE_TITLE FROM COURSE");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                IDs.add(rs.getString(1));
            }
        }
        catch(SQLException e){
            System.out.println(e);
        }
        return IDs;
    }
    public static ArrayList<String> listCompetencies(Connection conn){
        ArrayList<String> CompIDs = new ArrayList<String>();
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT COMP_ID, DESCRIPTION FROM COMPETENCY");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                CompIDs.add(rs.getString(1));
                System.out.println(CompIDs.size() + ". " + rs.getString(2));
            }
            
        }
        catch(SQLException e){
            System.out.println(e);
        }
        return CompIDs;
    }
    public static ArrayList<String> listElements(Connection conn, String compid){
        ArrayList<String> IDs = new ArrayList<String>();
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT ELEM_ID, NAME FROM COMPETENCY_ELEMENTS WHERE COMP_ID = ?");
            stmt.setString(1, compid);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                IDs.add(rs.getString(1));
                System.out.println(IDs.size() + ". " + rs.getString(2));
            }
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
        return IDs;
    }
    public static ArrayList<String> listCourseElements(Connection conn, String compid, String course_id){
        ArrayList<String> IDs = new ArrayList<String>();
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT ELEM_ID, NAME FROM COURSE_ELEMENTS WHERE COMP_ID = ? AND COURSE_ID = ?");
            stmt.setString(1, compid);
            stmt.setString(2, course_id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                IDs.add(rs.getString(1));
                System.out.println(IDs.size() + ". " + rs.getString(2));
            }
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
        return IDs;
    }

    public static ArrayList<String> listDomains(Connection conn){
        ArrayList<String> IDs = new ArrayList<String>();
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT DOMAIN_ID, TITLE FROM DOMAIN");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                IDs.add(rs.getString(1));
                System.out.println(IDs.size() + ". " + rs.getString(2));
            }
            SqlCLI.setCurrentException(null);
        }
        catch(SQLException e){
            SqlCLI.setCurrentException(e);
        }
        return IDs;
    }
    public static ArrayList<String> listTerms(Connection conn){
        ArrayList<String> IDs = new ArrayList<String>();
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM TERM");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                IDs.add(rs.getString(1));
                System.out.println(IDs.size() + ". " + rs.getString(2) + ", " + rs.getString(3));
            }
            
        }
        catch(SQLException e){
            System.out.println(e);
        }
        return IDs;
    }
    public static ArrayList<String> listClassTypes(Connection conn){
        ArrayList<String> IDs = new ArrayList<String>();
        try{
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM CLASSTYPE");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                IDs.add(rs.getString(1));
                System.out.println(IDs.size() + ". " + rs.getString(2));
            }
            
        }
        catch(SQLException e){
            System.out.println(e);
        }
        return IDs;
    }
}
