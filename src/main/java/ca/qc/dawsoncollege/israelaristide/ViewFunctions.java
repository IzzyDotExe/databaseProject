package ca.qc.dawsoncollege.israelaristide;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class ViewFunctions {

    public static void displayCompInfo(Connection conn, String comp_id) {

        try {

            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM COMPETENCY_INFO WHERE COMP_ID = ?");
            stmt.setString(1, comp_id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                System.out.println(rs.getString("COMP_ID") + " (" + rs.getString("ELEMENTS") + " Elements)");
                System.out.println("Competency Description: " + rs.getString("DESCRIPTION"));
            }

            PreparedStatement stmt2 = conn.prepareStatement("SELECT * FROM COMPETENCY_COURSES WHERE COMP_ID = ?");
            stmt2.setString(1, comp_id);
            ResultSet rs2 = stmt2.executeQuery();

            while (rs2.next()) {
                System.out.println(rs2.getString("COURSES") + " Courses use this competency");
            }

            rs.close();
            rs2.close();
            stmt.close();
            stmt2.close();

            SqlCLI.setCurrentException(null);

        } catch (SQLException ex) {

            SqlCLI.setCurrentException(ex);

        }

    }

    public static void displayCourseInfo(Connection conn, String course_id) {
        try {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM COURSE_INFO WHERE COURSEID = ?");
            statement.setString(1, course_id);
            ResultSet rs = statement.executeQuery();
            int hours = 0;
            while (rs.next()) {

                System.out.println(course_id + " (" + rs.getString("SEMESTER") + ")");
                System.out.println("Course title: " + rs.getString("TITLE"));
                System.out.println("Course domain: " + rs.getString("DOMAIN"));
                System.out.println("Course type: " + rs.getString("TYPE"));
                hours = rs.getInt("HOURS");
            }

            PreparedStatement statement1 = conn.prepareStatement("SELECT * FROM COURSE_DATA WHERE COURSE_ID = ?");
            statement1.setString(1, course_id);
            ResultSet rs1 = statement1.executeQuery();
            while (rs1.next()) {
                System.out.println(rs1.getInt("ELEMENT_HOURS") + "/" + hours + " Hours filled with " + rs1.getInt("ELEMENTS") + " elements.");
            }
            SqlCLI.setCurrentException(null);
        } catch (SQLException ex) {
            SqlCLI.setCurrentException(ex);
        }

    }

    public static void displayCourseElements(Connection conn, String course_id) {
        try {

            PreparedStatement statement = conn.prepareStatement("SELECT * FROM COURSE_ELEMENTS WHERE COURSE_ID = ? ORDER BY COMPETENCY");
            statement.setString(1, course_id);
            ResultSet rs = statement.executeQuery();

            String comp = "";
            while (rs.next()) {
                if (!rs.getString("COMPETENCY").equals(comp)) {
                    comp = rs.getString("COMPETENCY");
                    System.out.println(course_id + " Contains the following elements from " + comp + ":\n");
                }

                System.out.println(rs.getString("NAME"));
                System.out.println(rs.getString("DESCRIPTION"));
            }

            rs.close();
            statement.close();

            SqlCLI.setCurrentException(null);

        } catch (SQLException ex)  {

            SqlCLI.setCurrentException(ex);

        }
    }

}
