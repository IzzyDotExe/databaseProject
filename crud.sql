--------------------------------------------------------
--  DDL for Package CRUD
--------------------------------------------------------

CREATE OR REPLACE PACKAGE CRUD AS


     -- CREATE PROCEDURES


    PROCEDURE create_classtype(
        in_ct ctType
    );

    PROCEDURE create_term(
        in_term termType
    );

    PROCEDURE create_domain(
        in_domain domainType
    );

    PROCEDURE create_competency(
        in_comp competencyType
    );

    PROCEDURE create_element(
        in_comp VARCHAR,
        in_elem elementType
    );

    PROCEDURE create_course(
        in_course courseType
    );

    PROCEDURE create_course_element (
        in_course_id  VARCHAR,
        in_comp_id    VARCHAR,
        in_indx       NUMBER,
        in_hours      NUMBER
    );

    PROCEDURE create_course_elements (
        in_course_id VARCHAR,
        in_comp_id   VARCHAR,
        in_hours     NUMBER,
        in_elements  INTARR
    );


     -- DELETE PROCEDURES


    PROCEDURE delete_classtype (
        in_type_id VARCHAR
    );

    PROCEDURE delete_term (
        in_term_id VARCHAR
    );

    PROCEDURE delete_domain (
        in_domain_id VARCHAR
    );

    PROCEDURE delete_competency (
        in_comp_id VARCHAR
    );

    PROCEDURE delete_element (
        in_element_id VARCHAR
    );

    PROCEDURE delete_course (
        in_course_id VARCHAR
    );

    PROCEDURE delete_course_element (
        in_course_id  VARCHAR,
        in_comp_id    VARCHAR,
        in_indx       NUMBER
    );

    PROCEDURE delete_course_elements (
        in_course_id VARCHAR,
        in_comp_id   VARCHAR,
        in_elements  INTARR
    );


     -- UPDATE PROCEDURES


    PROCEDURE update_classtype(
        in_ct ctType
    );

    PROCEDURE update_term(
        in_term termType
    );

    PROCEDURE update_domain(
        in_domain domainType
    );

    PROCEDURE update_competency(
        in_comp competencyType
    );

    PROCEDURE update_element(
        in_elem elementType
    );

    PROCEDURE update_course(
        in_course courseType
    );

END CRUD;
/

CREATE OR REPLACE PACKAGE BODY CRUD
AS

     -- CREATE PROCEDURES

    PROCEDURE create_classtype (
        in_ct ctType
    ) IS

        id varchar(6);
    BEGIN

        SELECT utilities.generate_id('TYP', COUNT(*)) into id FROM CLASSTYPE;

        INSERT INTO ClassType (type_id, title)
            VALUES ( id, in_ct.title );

    END;

    PROCEDURE create_term (
        in_term termType
    ) IS
        id varchar(6);
    BEGIN

        SELECT utilities.generate_id('TRM', COUNT(*)) INTO id FROM TERM;

        INSERT INTO Term(term_id, season, year)
            VALUES (id, in_term.season, in_term.year);

    END;

    PROCEDURE create_domain (
        in_domain domainType
    ) IS

        id varchar(6);

    BEGIN

        SELECT utilities.generate_id('DOM', COUNT(*)) INTO id FROM DOMAIN;

        INSERT INTO Domain ( domain_id, title )
            VALUES (id, in_domain.title );

    END;

    PROCEDURE create_competency (
        in_comp competencyType
    ) IS

        id varchar(6);

    BEGIN

        SELECT utilities.generate_id('CMP', COUNT(*)) INTO id FROM Competency;

        INSERT INTO Competency (comp_id, description)
            VALUES (in_comp.comp_id, in_comp.description);

    END;

    PROCEDURE create_element(
        in_comp VARCHAR,
        in_elem elementType
    ) IS

        indx number(2);
        id varchar(6);

    BEGIN

        SELECT COUNT(*) + 1 INTO indx FROM CompElem
            WHERE comp_id = in_comp;

        SELECT utilities.generate_id('ELM', COUNT(*)) INTO id FROM Elements;

        INSERT INTO ELEMENTS (elem_id, name, description)
            VALUES (id, in_elem.name, in_elem.description);

        INSERT INTO CompElem(comp_id, elem_id, compelem_index)
            VALUES (in_comp, id, indx);

    END;

    PROCEDURE create_course (
        in_course courseType
    ) IS

    BEGIN

        INSERT INTO Course (

            course_id,
            course_description,
            course_title,
            course_hours,
            ponderation,
            credits,
            term_id,
            domain_id,
            type_id

        ) VALUES (

            in_course.course_id,
            in_course.course_description,
            in_course.course_title,
            in_course.course_hours,
            in_course.ponderation,
            in_course.credits,
            in_course.term_id,
            in_course.domain_id,
            in_course.type_id

        );

    END;

    PROCEDURE create_course_element (
        in_course_id  VARCHAR,
        in_comp_id    VARCHAR,
        in_indx       NUMBER,
        in_hours      NUMBER
    ) IS

        v_elem_id VARCHAR(6);

    BEGIN

        v_elem_id := utilities.fetch_element_from_index(in_comp_id, in_indx);

        INSERT INTO
        COURSEELEM (
            course_id,
            elem_id,
            hours
        ) VALUES (
            in_course_id,
            v_elem_id,
            in_hours
        );

    END;

    PROCEDURE create_course_elements (
        in_course_id VARCHAR,
        in_comp_id   VARCHAR,
        in_hours     NUMBER,
        in_elements  INTARR
    ) IS
        v_elem_each NUMBER;
    BEGIN
        v_elem_each := in_hours / in_elements.COUNT;
        FOR i IN 1 .. in_elements.count
        LOOP
            create_course_element(in_course_id, in_comp_id, i, v_elem_each);
        END LOOP;

    END;

     -- DELETE PROCEDURES

    PROCEDURE delete_classtype (
        in_type_id VARCHAR
    ) IS
    BEGIN
        DELETE FROM CLASSTYPE WHERE TYPE_ID = in_type_id;
    END;

    PROCEDURE delete_term (
        in_term_id VARCHAR
    ) IS
    BEGIN
        DELETE FROM TERM WHERE TERM_ID = in_term_id;
    END;

    PROCEDURE delete_domain (
        in_domain_id VARCHAR
    ) IS
    BEGIN
        DELETE FROM DOMAIN WHERE DOMAIN_ID = in_domain_id;
    END;

    PROCEDURE delete_competency (
        in_comp_id VARCHAR
    ) IS
    BEGIN
        DELETE FROM COMPETENCY WHERE COMP_ID = in_comp_id;
    END;

    PROCEDURE delete_element (
        in_element_id VARCHAR
    ) IS
    BEGIN
        DELETE FROM COMPELEM WHERE ELEM_ID = in_element_id;
        DELETE FROM ELEMENTS WHERE ELEM_ID = in_element_id;
    END;

    PROCEDURE delete_course (
        in_course_id VARCHAR
    ) IS
    BEGIN
        DELETE FROM COURSE WHERE COURSE_ID = in_course_id;
    END;

    PROCEDURE delete_course_element (
        in_course_id  VARCHAR,
        in_comp_id    VARCHAR,
        in_indx       NUMBER
    ) IS
        v_elem_id VARCHAR(6);
    BEGIN

        v_elem_id := utilities.fetch_element_from_index(in_comp_id, in_indx);

        DELETE FROM COURSEELEM
            WHERE ELEM_ID = v_elem_id
        AND COURSE_ID = in_course_id;

    END;

    PROCEDURE delete_course_elements (
        in_course_id VARCHAR,
        in_comp_id   VARCHAR,
        in_elements  INTARR
    ) IS

    BEGIN
        FOR i IN 1 .. in_elements.count
            LOOP
                delete_course_element(in_course_id, in_comp_id, i);
        END LOOP;
    END;

     -- UPDATE PROCEDURES

    PROCEDURE update_classtype(
        in_ct ctType
    ) IS

    BEGIN
        UPDATE CLASSTYPE
            SET TITLE = in_ct.TITLE
        WHERE TYPE_ID = in_ct.TYPE_ID;
    END;

    PROCEDURE update_term(
        in_term termType
    ) IS

    BEGIN

        UPDATE TERM
            SET SEASON = in_term.SEASON,
                YEAR = in_term.YEAR
        WHERE TERM_ID = in_term.TERM_ID;

    END;

    PROCEDURE update_domain(
        in_domain domainType
    ) IS

    BEGIN

        UPDATE DOMAIN
            SET TITLE = in_domain.TITLE
        WHERE DOMAIN_ID = in_domain.DOMAIN_ID;

    END;

    PROCEDURE update_competency(
        in_comp competencyType
    ) IS

    BEGIN

        UPDATE COMPETENCY
            SET DESCRIPTION = in_comp.DESCRIPTION
        WHERE COMP_ID = in_comp.COMP_ID;

    END;

    PROCEDURE update_element(
        in_elem elementType
    ) IS

    BEGIN

        UPDATE ELEMENTS
            SET NAME = in_elem.NAME,
                DESCRIPTION = in_elem.DESCRIPTION
        WHERE ELEM_ID = in_elem.ELEM_ID;

    END;

    PROCEDURE update_course(
        in_course courseType
    ) IS

    BEGIN

        UPDATE COURSE
            SET COURSE_DESCRIPTION = in_course.COURSE_DESCRIPTION,
                COURSE_TITLE = in_course.COURSE_TITLE,
                COURSE_HOURS = in_course.COURSE_HOURS,
                PONDERATION = in_course.PONDERATION,
                CREDITS = in_course.CREDITS,
                TERM_ID = in_course.TERM_ID,
                DOMAIN_ID = in_course.DOMAIN_ID,
                TYPE_ID = in_course.TYPE_ID
        WHERE COURSE_ID = in_course.COURSE_ID;

    END;

END CRUD;
/