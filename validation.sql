--------------------------------------------------------
--  File created - Thursday-November-24-2022   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body VALIDATION
--------------------------------------------------------

CREATE OR REPLACE PACKAGE VALIDATION AS 

    FUNCTION validate_course (
        in_hours       NUMBER,
        in_ponderation VARCHAR,
        in_credits     NUMBER
    ) RETURN NUMBER;

    FUNCTION validate_cid (
        in_course_id VARCHAR
    ) RETURN NUMBER;

    FUNCTION validate_element_hours (
        in_course_id VARCHAR
    ) RETURN NUMBER;

    FUNCTION expected_credits (
        in_course_id VARCHAR
    ) RETURN NUMBER;

    FUNCTION expected_hours (
        in_course_id VARCHAR
    ) RETURN NUMBER;

END VALIDATION;
/

CREATE OR REPLACE PACKAGE BODY VALIDATION AS

    FUNCTION validate_course (
        in_hours       NUMBER,
        in_ponderation VARCHAR,
        in_credits     NUMBER
    ) RETURN NUMBER AS

        CURSOR substr IS
        SELECT
            regexp_substr(in_ponderation, '[^-]+', 1, level) AS sub
        FROM
            dual
        CONNECT BY
            regexp_substr(in_ponderation, '[^-]+', 1, level) IS NOT NULL;

        calculatedhours NUMBER(3) := 0;
        credits         NUMBER(3) := 0;
        creditsdec      NUMBER;
        indx            NUMBER(1) := 0;
        
    BEGIN
        FOR item IN substr LOOP
            IF indx != 2 THEN
                calculatedhours := calculatedhours + to_number(item.sub);
            END IF;

            credits := credits + to_number(item.sub);
            indx := indx + 1;
        END LOOP;

        calculatedhours := calculatedhours * 15;
        creditsdec := credits / 3;
        IF (in_credits = creditsdec AND calculatedhours = in_hours)
        THEN
            return 1;
        ELSE
            return 0;
        END IF;
        
    END;

    FUNCTION validate_element_hours (
        in_course_id VARCHAR
    ) RETURN NUMBER AS
        v_course_hours NUMBER;
        v_elem_hours NUMBER;
    BEGIN

        SELECT COURSE_HOURS INTO v_course_hours FROM COURSE WHERE COURSE_ID = in_course_id;
        SELECT ELEMENT_HOURS INTO v_elem_hours FROM COURSE_DATA WHERE COURSE_ID = in_course_id;

        IF (v_course_hours = v_elem_hours) THEN
            return 1;
        ELSE
            return 0;
        END IF;

    END;

    FUNCTION validate_cid (
        in_course_id VARCHAR
    ) RETURN NUMBER AS

        hours NUMBER;
        ponderation VARCHAR(5);
        credits NUMBER;

        BEGIN

        SELECT HOURS, PONDERATION, CREDITS INTO hours, ponderation, credits FROM COURSE WHERE COURSE_ID = in_course_id;

        return validate_course(hours, ponderation, credits);

    END;

    FUNCTION expected_credits (
        in_course_id VARCHAR
    ) RETURN NUMBER AS

        BEGIN

    END;

    FUNCTION expected_hours (
        in_course_id VARCHAR
    ) RETURN NUMBER AS

        CURSOR substr IS
            SELECT
                regexp_substr(UTILITIES.GET_PONDERATION(in_course_id), '[^-]+', 1, level) AS sub
            FROM
                dual
            CONNECT BY
                regexp_substr(UTILITIES.GET_PONDERATION(in_course_id), '[^-]+', 1, level) IS NOT NULL;

        calculatedhours NUMBER(3) := 0;
        indx NUMBER := 0;

    BEGIN

        FOR item IN substr LOOP
            IF indx != 2 THEN
                calculatedhours := calculatedhours + to_number(item.sub);
            END IF;
            indx := indx + 1;
        END LOOP;

        calculatedhours := calculatedhours * 15;
    END;
END VALIDATION;
/
