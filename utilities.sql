--------------------------------------------------------
--  File created - Thursday-November-24-2022   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body UTILITIES
--------------------------------------------------------

CREATE OR REPLACE PACKAGE UTILITIES
IS

    FUNCTION generate_id(
        in_prefix      VARCHAR,
        in_item_amount NUMBER
    ) RETURN VARCHAR;

    FUNCTION fetch_element_from_index(
        in_comp  VARCHAR,
        in_index NUMBER
    ) RETURN VARCHAR;

    function get_ponderation (
        in_course_id VARCHAR
    ) RETURN VARCHAR;

END UTILITIES;
/

CREATE OR REPLACE PACKAGE BODY UTILITIES
IS

    FUNCTION generate_id(in_prefix VARCHAR, in_item_amount NUMBER)RETURN VARCHAR
    AS
    
        final_id VARCHAR(6);
        idnum NUMBER := in_item_amount + 1;

    BEGIN

        IF idnum < 10 
        THEN
            final_id := in_prefix || '00' || idnum;
        ELSIF idnum < 99 AND idnum > 9
        THEN
            final_id := in_prefix || '0' || idnum; 
        ELSE
            final_id := in_prefix || idnum;
        END IF;

        return final_id;

    END;
    
    FUNCTION fetch_element_from_index(in_comp VARCHAR, in_index NUMBER) RETURN VARCHAR
    AS
        v_elem_id VARCHAR(6);
    BEGIN
    
        SELECT elem_id INTO v_elem_id FROM Competency
            join compelem using (comp_id)
            join elements using (elem_id)
        WHERE compelem_index = in_index
        AND comp_id = in_comp; 
    
        return v_elem_id;
        
    END;

    function get_ponderation (
        in_course_id VARCHAR
    ) RETURN VARCHAR AS

        out_pond VARCHAR(5);
        BEGIN

        SELECT PONDERATION into out_pond FROM COURSE WHERE COURSE_ID = in_course_id;

        return out_pond;

    END;

END UTILITIES;
/
